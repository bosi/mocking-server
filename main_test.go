package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"

	"syreclabs.com/go/faker"
)

func TestHandleStatus(t *testing.T) {
	t.Parallel()

	r := internalServer()
	rr := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodGet, "/status", nil)

	r.ServeHTTP(rr, req)
	require.Equal(t, http.StatusOK, rr.Code)
	require.Equal(t, "ok", rr.Body.String())
}

//nolint:paralleltest
func TestHandleDeleteAllMocks(t *testing.T) {
	r := internalServer()
	rr := httptest.NewRecorder()

	tmpMocks = []tmpMock{{ServerPort: faker.Lorem().Word()}, {ServerPort: faker.Lorem().Word()}}
	require.Len(t, tmpMocks, 2)
	req := httptest.NewRequest(http.MethodDelete, "/api/v1/mocks", nil)

	r.ServeHTTP(rr, req)
	require.Equal(t, http.StatusNoContent, rr.Code)
	require.Len(t, tmpMocks, 0)
}

//nolint:paralleltest
func TestHandleDeleteServerMocks(t *testing.T) {
	r := internalServer()
	rr := httptest.NewRecorder()

	p1 := faker.Lorem().Word()
	p2 := faker.Lorem().Word()
	tmpMocks = []tmpMock{{ServerPort: p1}, {ServerPort: p2}}
	require.Len(t, tmpMocks, 2)
	req := httptest.NewRequest(http.MethodDelete, fmt.Sprintf("/api/v1/mocks/%s", p1), nil)

	r.ServeHTTP(rr, req)
	require.Equal(t, http.StatusNoContent, rr.Code)
	require.Len(t, tmpMocks, 1)
	require.Equal(t, p2, tmpMocks[0].ServerPort)
}

//nolint:paralleltest
func TestHandleAddMock(t *testing.T) {
	r := internalServer()

	t.Run("check wrong port", func(t *testing.T) {
		rr := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodPost, fmt.Sprintf("/api/v1/mocks/%s", faker.Lorem().Word()), nil)
		r.ServeHTTP(rr, req)
		require.Equal(t, http.StatusBadRequest, rr.Code)
	})

	t.Run("check happy path", func(t *testing.T) {
		tmpMocks = []tmpMock{}

		rr := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodPost, "/api/v1/mocks/10001", strings.NewReader("{\"request\": {\"method\": \"GET\", \"url\": \"/test123\"}}"))
		req.Header.Set("content-type", "application/json")
		r.ServeHTTP(rr, req)
		require.Equal(t, http.StatusOK, rr.Code)
		require.Len(t, tmpMocks, 1)
		require.Equal(t, "/test123", tmpMocks[0].M.Request.URL)
	})

	t.Run("check happy path with override", func(t *testing.T) {
		tm1 := tmpMock{ServerPort: "10001", M: mock{Request: requestMock{Method: "POST", URL: "/t2"}}}
		tm2 := tmpMock{ServerPort: "10001", M: mock{Request: requestMock{Method: "GET", URL: "/t1"}}}
		tm3 := tmpMock{ServerPort: "10002", M: mock{Request: requestMock{Method: "GET", URL: "/t2"}}}

		tmpMocks = []tmpMock{
			tm1, tm2,
			{ServerPort: "10002", M: mock{Request: requestMock{Method: "POST", URL: "/t2"}, Response: responseMock{Body: "123"}}},
			tm3,
		}

		rr := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodPost, "/api/v1/mocks/10002", strings.NewReader("{\"request\": {\"method\": \"POST\", \"url\": \"/t2\"}, \"response\": {\"body\":\"456\"}}"))
		req.Header.Set("content-type", "application/json")
		r.ServeHTTP(rr, req)
		require.Equal(t, http.StatusOK, rr.Code)
		require.Len(t, tmpMocks, 4)
		require.Equal(t, tm1, tmpMocks[0])
		require.Equal(t, tm2, tmpMocks[1])
		require.Equal(t, "456", tmpMocks[2].M.Response.Body)
		require.Equal(t, tm3, tmpMocks[3])
	})
}
