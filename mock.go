package main

import (
	"log"
	"net/http"
	"net/url"
	"regexp"
	"strings"
)

type (
	mockConfig struct {
		Mocks []mock
	}

	mock struct {
		Request  requestMock `json:"request,omitempty"`
		Response responseMock
	}

	tmpMock struct {
		ServerPort string `json:"serverPort"`
		M          mock   `json:"mock"`
	}

	requestMock struct {
		Method      string `json:"method,omitempty"`
		URL         string `json:"url,omitempty"`
		BearerToken string `json:"bearerToken,omitempty"`
	}

	responseMock struct {
		Code    int               `json:"code,omitempty"`
		Body    string            `json:"body,omitempty"`
		File    string            `json:"file,omitempty"`
		Headers map[string]string `json:"headers,omitempty"`
	}
)

var tmpMocks = []tmpMock{}

func (m mock) match(r *http.Request) bool {
	if strings.ToLower(m.Request.Method) != strings.ToLower(r.Method) {
		return false
	}

	if !strings.HasPrefix(m.Request.URL, "/") {
		m.Request.URL = "/" + m.Request.URL
	}

	mockURL, _ := url.Parse(m.Request.URL)
	if mockURL.Path == r.URL.Path && mockURL.Query().Encode() == r.URL.Query().Encode() {
		return true
	}

	if strings.Count(mockURL.Path, "/") != strings.Count(r.URL.Path, "/") {
		return false
	}

	matchPath := matchURLPart(r.URL.Path, mockURL.Path)

	if mockURL.Query().Encode() != "" || r.URL.Query().Encode() != "" {
		if len(mockURL.Query()) != len(r.URL.Query()) {
			return false
		}

		matchQuery := matchURLPart(r.URL.Query().Encode(), mockURL.Query().Encode())

		return matchPath && matchQuery
	}
	return matchPath
}

func matchURLPart(s string, basePattern string) bool {
	re := "^" + basePattern + "$"
	re = strings.Replace(re, "%2A", ".+", -1) // replace encoded "*"
	re = strings.Replace(re, "*", ".+", -1)

	match, err := regexp.MatchString(re, s)
	if err != nil {
		log.Printf("cannot parse regex: %s", err.Error())
		return false
	}
	return match
}

func addOrReplaceTmpMock(tmNew tmpMock) {
	for i, tm := range tmpMocks {
		if tm.ServerPort == tmNew.ServerPort &&
			tm.M.Request.URL == tmNew.M.Request.URL &&
			tm.M.Request.Method == tmNew.M.Request.Method {
			tmpMocks[i] = tmNew
			return
		}
	}

	tmpMocks = append(tmpMocks, tmNew)
}
