package main

import (
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/labstack/echo/v4"
)

const (
	startupWaitPeriod = 100 * time.Millisecond
)

func main() {
	startMockServers()

	time.Sleep(startupWaitPeriod)
	log.Println("startup done")

	log.Fatal(http.ListenAndServe(":3060", logRequest(internalServer())))
}

func internalServer() *echo.Echo {
	r := echo.New()
	r.POST("/api/v1/mocks/:server", handleAddMock)
	r.DELETE("/api/v1/mocks/:server", handleDeleteMocksMocks)
	r.DELETE("/api/v1/mocks", handleDeleteAllMocks)
	r.GET("/status", handleStatus)
	return r
}

func handleStatus(c echo.Context) error {
	return c.String(http.StatusOK, "ok")
}

func handleDeleteAllMocks(c echo.Context) error {
	tmpMocks = []tmpMock{}
	return c.NoContent(http.StatusNoContent)
}

func handleDeleteMocksMocks(c echo.Context) error {
	newTmpMocks := []tmpMock{}

	serverParts := strings.Split(c.Param("server"), "-")
	serverPort := serverParts[0]

	for _, tm := range tmpMocks {
		if tm.ServerPort != serverPort {
			newTmpMocks = append(newTmpMocks, tm)
		}
	}

	tmpMocks = newTmpMocks
	return c.NoContent(http.StatusNoContent)
}

func handleAddMock(c echo.Context) error {
	m := mock{}
	if err := c.Bind(&m); err != nil {
		return c.String(http.StatusBadRequest, fmt.Sprintf("cannot bind request body to model: %s", err.Error()))
	}

	serverParts := strings.Split(c.Param("server"), "-")
	serverPort := serverParts[0]

	if !isExistingServerPort(serverPort) {
		return c.String(
			http.StatusBadRequest,
			fmt.Sprintf("serverPort %s is invalid. you have to select an existing server", serverPort),
		)
	}

	tm := tmpMock{
		ServerPort: serverPort,
		M:          m,
	}

	addOrReplaceTmpMock(tm)

	return c.JSON(http.StatusOK, tm)
}
