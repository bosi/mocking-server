package main

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestMockServer(t *testing.T) {
	t.Parallel()

	rc := routerContexts()[0]
	r := mockServer(rc)

	tcs := []struct {
		name         string
		method       string
		expectStatus int
	}{
		{name: "check get", method: http.MethodGet, expectStatus: http.StatusOK},
		{name: "check post", method: http.MethodPost, expectStatus: http.StatusNotImplemented},
		{name: "check put", method: http.MethodPut, expectStatus: http.StatusNotImplemented},
		{name: "check delete", method: http.MethodDelete, expectStatus: http.StatusNotImplemented},
	}

	for _, tc := range tcs {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			rr := httptest.NewRecorder()
			r.ServeHTTP(rr, httptest.NewRequest(tc.method, "/hello/world", nil))
			require.Equal(t, tc.expectStatus, rr.Code)
		})
	}
}
