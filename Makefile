include project-templates/base.mk

project-templates/base.mk:
	@cp -ar ~/.dotfiles/projects/golang ./project-templates

.env:
	touch .env

test:
	rm -rf mocks
	mkdir -p mocks
	cp -ar example/* mocks/
	@${GOPATH}/bin/gotest ./...
