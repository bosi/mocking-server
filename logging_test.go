package main

import (
	"bytes"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

//nolint:paralleltest
func TestLogRequest(t *testing.T) {
	r := http.NewServeMux()
	r.HandleFunc("/hello", func(w http.ResponseWriter, _ *http.Request) {
		w.WriteHeader(http.StatusTeapot)
	})

	output := bytes.NewBufferString("")
	log.SetOutput(output)

	req, err := http.NewRequest(http.MethodGet, "/hello", nil)
	require.Nil(t, err)
	rr := httptest.NewRecorder()

	logRequest(r).ServeHTTP(rr, req)

	logEntry := output.String()
	assert.Contains(t, logEntry, fmt.Sprint(http.MethodGet))
	assert.Contains(t, logEntry, "/hello")
	assert.Contains(t, logEntry, strconv.Itoa(http.StatusTeapot))
}
