package main

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"
	"syreclabs.com/go/faker"
)

//nolint:paralleltest
func TestRouterContext_Handle(t *testing.T) {
	tmpMocks = []tmpMock{}

	t.Run("check no config", func(t *testing.T) {
		rc := routerContext{path: faker.Lorem().Word()}
		req, err := http.NewRequest(http.MethodGet, "/something", nil)
		require.Nil(t, err)
		rr := httptest.NewRecorder()

		rc.handle(rr, req)
		require.Equal(t, http.StatusNotImplemented, rr.Code)
		require.Contains(t, rr.Body.String(), "cannot not determine config file path")
	})

	t.Run("check no mock matching", func(t *testing.T) {
		rc := routerContexts()[0]
		req, err := http.NewRequest(http.MethodGet, "/something", nil)
		require.Nil(t, err)
		rr := httptest.NewRecorder()

		rc.handle(rr, req)
		require.Equal(t, http.StatusNotImplemented, rr.Code)
		require.Contains(t, rr.Body.String(), "no mock matched")
	})

	t.Run("check happy case with json config", func(t *testing.T) {
		rc := routerContexts()[0]
		req, err := http.NewRequest(http.MethodGet, "/hello/world", nil)
		require.Nil(t, err)
		rr := httptest.NewRecorder()

		rc.handle(rr, req)
		require.Equal(t, http.StatusOK, rr.Code)
		require.Contains(t, rr.Body.String(), "hello")
	})

	t.Run("check happy case with yaml config", func(t *testing.T) {
		rc := routerContexts()[2]
		req, err := http.NewRequest(http.MethodGet, "/hello/penguin", nil)
		require.Nil(t, err)
		rr := httptest.NewRecorder()

		rc.handle(rr, req)
		require.Equal(t, http.StatusOK, rr.Code)
		require.Contains(t, rr.Body.String(), "penguin")
	})

	t.Run("check happy case with inline body", func(t *testing.T) {
		rc := routerContexts()[0]
		req, err := http.NewRequest(http.MethodGet, "/inline", nil)
		require.Nil(t, err)
		rr := httptest.NewRecorder()

		rc.handle(rr, req)
		require.Equal(t, http.StatusOK, rr.Code)
		require.Contains(t, rr.Body.String(), "hi there :)")
	})

	t.Run("check happy case without body", func(t *testing.T) {
		rc := routerContexts()[0]
		req, err := http.NewRequest(http.MethodGet, "/status", nil)
		require.Nil(t, err)
		rr := httptest.NewRecorder()

		rc.handle(rr, req)
		require.Equal(t, http.StatusNoContent, rr.Code)
		require.Equal(t, rr.Body.String(), "")
	})

	t.Run("check happy case with tmp mock", func(t *testing.T) {
		rc := routerContexts()[0]
		tmpMocks = []tmpMock{{
			ServerPort: rc.port,
			M: mock{
				Request:  requestMock{Method: "GET", URL: "/hello/world"},
				Response: responseMock{Code: http.StatusOK, Body: "i am a temp mock"},
			},
		}}
		req, err := http.NewRequest(http.MethodGet, "/hello/world", nil)
		require.Nil(t, err)
		rr := httptest.NewRecorder()

		rc.handle(rr, req)
		require.Equal(t, "i am a temp mock", rr.Body.String())
	})
}

func TestHandleMockedRoute(t *testing.T) {
	t.Parallel()

	mock := mock{
		Request: requestMock{
			BearerToken: faker.Lorem().String(),
		},
		Response: responseMock{
			Code: http.StatusTeapot,
			Headers: map[string]string{
				"X-Test-Header": faker.Lorem().String(),
			},
		},
	}

	rc := routerContext{}

	t.Run("check invalid credentials", func(t *testing.T) {
		t.Parallel()

		req := httptest.NewRequest(http.MethodGet, "/", nil)
		req.Header.Set("Authorization", "Bearer hello world")
		rr := httptest.NewRecorder()
		handleMockedRoute(mock, rc, context{r: req, w: rr})
		require.Equal(t, http.StatusForbidden, rr.Code)
		require.Equal(t, "", rr.Header().Get("X-Test-Header"))
	})

	t.Run("check valid credentials", func(t *testing.T) {
		t.Parallel()

		req := httptest.NewRequest(http.MethodGet, "/", nil)
		req.Header.Set("Authorization", "Bearer "+mock.Request.BearerToken)
		rr := httptest.NewRecorder()
		handleMockedRoute(mock, rc, context{r: req, w: rr})
		require.Equal(t, http.StatusTeapot, rr.Code)
		require.Equal(t, mock.Response.Headers["X-Test-Header"], rr.Header().Get("X-Test-Header"))
	})

	t.Run("check url with query", func(t *testing.T) {
		t.Parallel()

		mock.Request.URL = "?name=john"
		req := httptest.NewRequest(http.MethodGet, "/?name=john", nil)
		req.Header.Set("Authorization", "Bearer "+mock.Request.BearerToken)
		rr := httptest.NewRecorder()
		handleMockedRoute(mock, rc, context{r: req, w: rr})
		require.Equal(t, http.StatusTeapot, rr.Code)
	})
}
