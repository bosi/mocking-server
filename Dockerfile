ARG BINARY_NAME

############################
# STEP 1 build executable binary
############################
FROM golang:1.20.4-alpine3.16@sha256:6469405d7297f82d56195c90a3270b0806ef4bd897aa0628477d9959ab97a577 AS builder

ARG BINARY_NAME
WORKDIR ${GOPATH}/src/${BINARY_NAME}

RUN apk --no-cache add git ca-certificates make openssh bash && \
    mkdir -p ~/.ssh && \
    touch ~/.ssh/known_hosts && \
    ssh-keyscan gitlab.com > ~/.ssh/known_hosts

COPY . .

ARG CI_JOB_TOKEN=""
ENV CI=true
RUN make build && \
    make check-licenses > /dev/null

############################
# STEP 2 build a small image
############################
FROM alpine:3.21.3@sha256:a8560b36e8b8210634f77d9f7f9efd7ffa463e380b75e2e74aff4511df3ef88c

RUN apk --no-cache update && \
    apk --no-cache add curl && \
    adduser -D -H appuser && \
    mkdir /app

WORKDIR /app

ARG BINARY_NAME
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /go/src/${BINARY_NAME}/${BINARY_NAME} /app/
COPY --from=builder /go/src/${BINARY_NAME}/LICEN* /app/
COPY --from=builder /go/src/${BINARY_NAME}/README.md /app/

RUN ln -s /app/${BINARY_NAME} /app/app && \
    chown appuser:appuser -R /app

USER appuser

HEALTHCHECK --interval=5s \
            --timeout=5s \
            --start-period=5s \
            CMD curl -f http://127.0.0.1:3060/status || exit 1

EXPOSE 3060

ENTRYPOINT ["./app"]

