package main

import (
	"fmt"
	"log"
	"net/http"
	"runtime"
	"time"
)

type (
	responseMetaData struct {
		status int
	}

	loggingResponseWriter struct {
		http.ResponseWriter
		meta responseMetaData
	}
)

const (
	reset  = "\033[0m"
	red    = "\033[31m"
	green  = "\033[32m"
	yellow = "\033[33m"
	blue   = "\033[34m"
	purple = "\033[35m"
	cyan   = "\033[36m"
	white  = "\033[97m"

	requestDurationPrecision = time.Microsecond / 10
)

func (lw *loggingResponseWriter) WriteHeader(statusCode int) {
	lw.ResponseWriter.WriteHeader(statusCode) // write status code using original http.ResponseWriter
	lw.meta.status = statusCode               // capture status code
}

func logRequest(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		lw := &loggingResponseWriter{
			ResponseWriter: w,
			meta:           responseMetaData{status: http.StatusOK},
		}

		start := time.Now()
		handler.ServeHTTP(lw, r)

		log.Printf(
			" | %s | %10.10s | %s | %s%s\n",
			coloredStatusCode(lw.meta.status),
			time.Since(start).Round(requestDurationPrecision).String(),
			coloredMethod(r.Method),
			r.Host,
			r.URL.String(),
		)
	})
}

func coloredStatusCode(code int) string {
	if runtime.GOOS == "windows" {
		return fmt.Sprintf("%d", code)
	}

	var color string
	switch {
	case code < http.StatusMultipleChoices:
		color = green
	case code < http.StatusBadRequest:
		color = yellow
	default:
		color = red
	}

	return fmt.Sprintf("%s%d%s", color, code, reset)
}

func coloredMethod(method string) string {
	if runtime.GOOS == "windows" {
		return fmt.Sprintf("%6.6s", method)
	}

	var color string
	switch method {
	case http.MethodPost:
		color = blue
	case http.MethodPut:
		color = purple
	case http.MethodPatch:
		color = cyan
	case http.MethodDelete:
		color = red
	default:
		color = white
	}

	return fmt.Sprintf("%s%6.6s%s", color, method, reset)
}
