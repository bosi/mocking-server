package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"

	"gopkg.in/yaml.v3"
)

type routerContext struct {
	name string
	port string
	path string
}

const (
	mockDir            = "mocks"
	mockServerDirParts = 2
)

var allowedConfNames = []string{
	"config.json",
	"config.yaml",
	"config.yml",
	"config",
}

func routerContexts() []routerContext {
	var rcs []routerContext

	dirInfos, _ := ioutil.ReadDir(mockDir)
	for _, dirInfo := range dirInfos {
		if !dirInfo.IsDir() {
			continue
		}

		nameSplit := strings.Split(dirInfo.Name(), "-")

		if len(nameSplit) != mockServerDirParts {
			log.Printf("mock-server directory name \"%s\" name must contain exaktly one \"-\": skipping", dirInfo.Name())
			continue
		}

		if _, err := strconv.Atoi(nameSplit[0]); err != nil {
			log.Printf("first part of mock-server directory name \"%s\" must contain a number: skipping", dirInfo.Name())
			continue
		}

		rcs = append(rcs, routerContext{
			name: nameSplit[1],
			port: nameSplit[0],
			path: mockDir + "/" + dirInfo.Name(),
		})
	}

	return rcs
}

func (rc routerContext) determineConfigPath() (string, error) {
	for _, name := range allowedConfNames {
		configFilePath := fmt.Sprintf("%s/%s", rc.path, name)
		if _, err := os.Stat(configFilePath); err == nil {
			return configFilePath, nil
		}
	}

	return "", fmt.Errorf("no config file found in \"%s\"", rc.path)
}

func (rc routerContext) mockConfig() (mockConfig, error) {
	configFilePath, err := rc.determineConfigPath()
	if err != nil {
		return mockConfig{}, fmt.Errorf("cannot not determine config file path: %s", err)
	}

	configFile, err := os.Open(configFilePath)
	if err != nil {
		return mockConfig{}, fmt.Errorf("cannot open config file: %s", err)
	}

	var jsonErr, yamlErr error

	mc := mockConfig{}

	jsonErr = json.NewDecoder(configFile).Decode(&mc)
	if jsonErr == nil {
		return mc, nil
	}

	//nolint:errcheck
	configFile.Seek(0, 0)

	yamlErr = yaml.NewDecoder(configFile).Decode(&mc)
	if yamlErr == nil {
		return mc, nil
	}

	return mockConfig{}, fmt.Errorf(
		"cannot parse config (path: %s) as json (error: %s) or yaml (error: %s)",
		configFilePath,
		jsonErr,
		yamlErr,
	)
}

func (rc routerContext) tmpMocks() []mock {
	mocks := []mock{}
	for _, tm := range tmpMocks {
		if tm.ServerPort == rc.port {
			mocks = append(mocks, tm.M)
		}
	}

	return mocks
}

func isExistingServerPort(serverPort string) bool {
	for _, rc := range routerContexts() {
		if rc.port == serverPort {
			return true
		}
	}

	return false
}
