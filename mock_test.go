package main

import (
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestMock(t *testing.T) {
	t.Parallel()

	tcs := []struct {
		mockURL    string
		mockMethod string
		reqURL     string
		reqMethod  string
		expect     bool
	}{
		{
			mockURL: "/hello/world",
			reqURL:  "/hello/world",
			expect:  true,
		},
		{
			mockURL: "hello/world",
			reqURL:  "/hello/world",
			expect:  true,
		},
		{
			mockURL:    "/hello/world",
			mockMethod: "Get",
			reqURL:     "/hello/world",
			reqMethod:  "geT",
			expect:     true,
		},
		{
			mockURL: "/hello",
			reqURL:  "/hello/world",
			expect:  false,
		},
		{
			mockURL: "/hello/world",
			reqURL:  "/hello",
			expect:  false,
		},
		{
			mockURL:    "/hello/world",
			mockMethod: http.MethodPost,
			reqURL:     "/hello/world",
			expect:     false,
		},
		{
			mockURL:   "/hello/world",
			reqURL:    "/hello/world",
			reqMethod: http.MethodPost,
			expect:    false,
		},
		{
			mockURL: "/hello?name=john",
			reqURL:  "/hello?name=john",
			expect:  true,
		},
		{
			mockURL: "/hello?first_name=john&last_name=doe",
			reqURL:  "/hello?first_name=john&last_name=doe",
			expect:  true,
		},
		{
			mockURL: "/hello?first_name=john",
			reqURL:  "/hello?first_name=john&last_name=doe",
			expect:  false,
		},
		{
			mockURL: "/hello?first_name=john&last_name=doe",
			reqURL:  "/hello?last_name=doe",
			expect:  false,
		},
		{
			mockURL: "/hello?first_name=john&last_name=doe",
			reqURL:  "/hello?last_name=doe&first_name=john",
			expect:  true,
		},
		{
			mockURL: "/hello?first_name=*&last_name=*",
			reqURL:  "/hello?last_name=doe&first_name=john42",
			expect:  true,
		},
		{
			mockURL: "/hello?first_name=*&last_name=*",
			reqURL:  "/hello?first_name=john42",
			expect:  false,
		},
		{
			mockURL: "/hello/*",
			reqURL:  "/hello/world",
			expect:  true,
		},
		{
			mockURL: "/hello/*",
			reqURL:  "/hello/beautiful/world",
			expect:  false,
		},
		{
			mockURL: "/hello/[0-9]+",
			reqURL:  "/hello/world",
			expect:  false,
		},
		{
			mockURL: "/hello/[0-9]+",
			reqURL:  "/hello/42",
			expect:  true,
		},
		{
			mockURL: "/hello/\\d+",
			reqURL:  "/hello/42",
			expect:  true,
		},
		{
			mockURL: "/hello/\\d+|hello",
			reqURL:  "/hello/42",
			expect:  true,
		},
		{
			mockURL: "/hello/universe|world",
			reqURL:  "/hello/world",
			expect:  true,
		},
		{
			mockURL: "/hello/universe|world",
			reqURL:  "/hello/universe",
			expect:  true,
		},
		{
			mockURL: "/hello/universe|world",
			reqURL:  "/hello/universe|world",
			expect:  true,
		},
		{
			mockURL: "/hello/\\d+|world",
			reqURL:  "/hello/world",
			expect:  true,
		},
		{
			mockURL: "/hello/(\\d+|world)",
			reqURL:  "/hello/42|world",
			expect:  false,
		},
		{
			mockURL: "/hello/\\d+\\|world",
			reqURL:  "/hello/42|world",
			expect:  true,
		},
	}

	for i, tc := range tcs {
		tc := tc
		t.Run("check testcase no "+strconv.Itoa(i), func(t *testing.T) {
			t.Parallel()

			reqMethod := tc.reqMethod
			if reqMethod == "" {
				reqMethod = http.MethodGet
			}

			mockMethod := tc.mockMethod
			if mockMethod == "" {
				mockMethod = http.MethodGet
			}

			req := httptest.NewRequest(reqMethod, tc.reqURL, nil)
			m := mock{Request: requestMock{Method: mockMethod, URL: tc.mockURL}}
			require.Equal(t, tc.expect, m.match(req))
		})
	}
}
