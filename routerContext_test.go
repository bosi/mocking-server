package main

import (
	"os"
	"testing"

	"syreclabs.com/go/faker"

	"github.com/stretchr/testify/require"
)

func TestRouterContexts(t *testing.T) {
	t.Run("check valid config", func(t *testing.T) {
		t.Parallel()

		rcs := routerContexts()
		require.Len(t, rcs, 3)
		require.Equal(t, "server1", rcs[0].name)
		require.Equal(t, "10001", rcs[0].port)
		require.Equal(t, "mocks/10001-server1", rcs[0].path)
	})

	t.Run("check dir with invalid names is not loaded", func(t *testing.T) {
		t.Parallel()

		require.Nil(t, os.Mkdir("mocks/10003-invalid-name", os.ModePerm))
		require.Len(t, routerContexts(), 3)
		require.Nil(t, os.Remove("mocks/10003-invalid-name"))
	})

	t.Run("check dir with invalid port number is not loaded", func(t *testing.T) {
		t.Parallel()

		require.Nil(t, os.Mkdir("mocks/abc-hello", os.ModePerm))
		require.Len(t, routerContexts(), 3)
		require.Nil(t, os.Remove("mocks/abc-hello"))
	})
}

//nolint:paralleltest
func TestRouterContext_TmpMocks(t *testing.T) {
	p1 := faker.Lorem().Word()
	p2 := faker.Lorem().Word()

	tmpMocks = []tmpMock{
		{ServerPort: p1, M: mock{Request: requestMock{URL: "/1"}}},
		{ServerPort: p1, M: mock{Request: requestMock{URL: "/2"}}},
		{ServerPort: p2, M: mock{Request: requestMock{URL: "/3"}}},
		{ServerPort: p2, M: mock{Request: requestMock{URL: "/4"}}},
		{ServerPort: p1, M: mock{Request: requestMock{URL: "/5"}}},
	}

	serverTmpMocks := routerContext{port: p1}.tmpMocks()
	require.Equal(t, "/1", serverTmpMocks[0].Request.URL)
	require.Equal(t, "/2", serverTmpMocks[1].Request.URL)
	require.Equal(t, "/5", serverTmpMocks[2].Request.URL)
}
