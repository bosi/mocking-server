package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
)

type context struct {
	w http.ResponseWriter
	r *http.Request
}

func (rc routerContext) handle(w http.ResponseWriter, r *http.Request) {
	c := context{w: w, r: r}

	mc, err := rc.mockConfig()
	if err != nil {
		rc.sendError(c, err)
		return
	}

	mocks := append(rc.tmpMocks(), mc.Mocks...)

	for _, m := range mocks {
		if m.match(r) {
			handleMockedRoute(m, rc, c)
			return
		}
	}

	rc.sendError(c, errors.New("you have called the mocking server but no mock matched your request"))
}

func (rc routerContext) sendError(c context, err error) {
	c.w.Header().Set("content-type", "application/json")
	d := map[string]interface{}{
		"message":       err.Error(),
		"serverName":    rc.name,
		"serverPort":    rc.port,
		"requestHost":   c.r.Host,
		"requestUrl":    c.r.RequestURI,
		"requestMethod": c.r.Method,
	}
	c.w.WriteHeader(http.StatusNotImplemented)
	_ = json.NewEncoder(c.w).Encode(d)
}

func handleMockedRoute(mock mock, rc routerContext, c context) {
	if mock.Request.BearerToken != "" {
		if "Bearer "+mock.Request.BearerToken != c.r.Header.Get("Authorization") {
			c.w.WriteHeader(http.StatusForbidden)
			return
		}
	}

	for key, value := range mock.Response.Headers {
		c.w.Header().Set(key, value)
	}

	c.w.WriteHeader(mock.Response.Code)

	if mock.Response.Body != "" {
		if _, err := fmt.Fprint(c.w, mock.Response.Body); err != nil {
			log.Printf("cannot copy data from inline to response: %s", err.Error())
		}

		return
	}

	if mock.Response.File != "" {
		respFile, _ := os.Open(rc.path + "/" + mock.Response.File)
		if _, err := io.Copy(c.w, respFile); err != nil {
			log.Printf("cannot copy data from file to response: %s", err.Error())
		}
	}
}
