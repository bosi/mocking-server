package main

import (
	"log"
	"net/http"
)

func startMockServers() {
	rcs := routerContexts()

	for _, rc := range rcs {
		go startMockServer(rc)
	}
}

func startMockServer(rc routerContext) {
	r := mockServer(rc)
	log.Printf("start server \"%s\" at port %s\n", rc.name, rc.port)
	log.Fatal(http.ListenAndServe(":"+rc.port, logRequest(r)))
}

func mockServer(rc routerContext) *http.ServeMux {
	r := http.NewServeMux()
	r.HandleFunc("/", rc.handle)

	return r
}
