# Mocking Server

[![pipeline status](https://gitlab.com/bosi/mocking-server/badges/master/pipeline.svg)](https://gitlab.com/bosi/simple-node-exporter/-/commits/master)
[![coverage report](https://gitlab.com/bosi/mocking-server/badges/master/coverage.svg)](https://gitlab.com/bosi/simple-node-exporter/-/commits/master)

A mocking exporter for http apis.

## General facts

### Why I start this project?

There are several mocking servers out there, but none of them meet my requirements.

### What was the goal of the project?

Create a simple mocking server with multiple configuration options that can double the api endpoints. It should be
possible to easily save the complete configuration via git.

### What exactly does the tool?

Without configuration: nothing :)

You can create as many ridicule servers as you like and define methods, urls, authentication, responses, headers, and
more for your ridiculed requests. Each server listens on its own port.

## Usage

Just clone this repository and start the docker container using `docker-compose`:

```
git clone https://gitlab.com/bosi/mocking-server
cd mocking-server
docker-compose up
```

You may now call the endpoints:

```
curl http://localhost:10001/hello/world
curl http://localhost:10001/secured -H "Authorization: Bearer mytoken" -X POST
curl http://localhost:10002/hello/universe
```

### Add new server

1. create a new directory inside your `mocks` directory with the following pattern: `[port]-[name]`
    ```
    mkdir mocks/10005-fancy_server
    ```
2. create a config file: `vim mocks/10005-fancy_server/config.json`
    ```json
    {
        "mocks": [
            {
                "request": {
                    "method": "GET",
                    "url": "/hello/world"
                },
                "response": {
                    "code": 200,
                    "file": "hello_world.json",
                    "headers": {
                        "Content-Type": "application/json"
                    }
                }
            }
        ]
    }
    ```
3. create a dummy response: `vim mocks/10005-fancy_server/resp1.json`
    ```json
    {
        "hello": "world"
    }
    ```
4. start the server using e.g. docker:
    ```
    docker run --rm -v ${PWD}/mocks:/app/mocks -p 10001:10001 bosix/mocking-server
    ``` 

### Configuration

The config file inside a server must have one of the following names: `config.json`, `config.yaml`, `config.yml` or
`config`. It can contain json or yaml, so you can choose what you like better. You may find an example for both variants
inside the `example/` dir.

### Add authentication

You can set a http bearer token the endpoint expects when calling it. If you do not provide the token or provide an
incorrect token, the server responds with http 403.

Adjust the `config.json` like the following:

```json
    {
    "mocks": [
        {
            "request": {
                "method": "GET",
                "url": "/hello/world",
                "bearerToken": "mytoken"
            }
            // [...]
        }
    ]
}
```

### Query-Parameters

It is possible to set expected query-parameters like the following:

```json
{
    //[...]
    "request": {
        "method": "GET",
        "url": "/hello/world?first_name=john&last_name=doe"
    }
    //[...]
}
```

Please take notice that this mock matches with any order of params e.g. `/hello/world?first_name=john&last_name=doe`
or `/hello/world?last_name=doe&first_name=john`

### Inline Body

If you do not want to create a file which contains the response you can define it inline inside the configuration as
well:

```json
{
    // [...]
    "response": {
        "code": 200,
        "body": "{\"msg\":\"hi there :)\"}",
        "headers": {
            "Content-Type": "application/json"
        }
    }
    // [...] 
}
```

The value of `body` has to be a string.

### Wildcards

You can use wildcards to match a given url/query. Example (request url): `/hello?first_name=*&last_name=*` or
`/hello/*`. The mocks are checked in the order they are defined in the `config.json` and the first mock that matches the
request is used for the response.

### Change configuration at runtime

You can change the configuration and any response file at any time without restarting. The files are parsed on every
request. If you want to add/remove a server, you have to restart the application.

### Change Mocks via API

Sometimes it is easier to change or set a mock via an API call, for example from within a running automated test. You
can create new mocks or modify existing mocks by using the mocking server api:

```
curl --request POST \
  --url http://localhost:3060/api/v1/mocks/10001 \
  --header 'Content-Type: application/json' \
  --data '{
	"request": {
		"method": "GET",
		"url": "/a-temp-mock"
	},
	"response": {
		"code": 200,
		"body": "hi i am temporary :)"
	}
}'
```

The created or modified mock is available until the next restart of the mocking server. The payload allows everything
you can do in the server configuration file and the URL takes the server port (`10001` in the example).

If necessary, you can also delete temp mocks:

- all temp mocks: `curl --request DELETE --url http://localhost:3060/api/v1/mocks`
- temp mocks of one server: `curl --request DELETE --url http://localhost:3060/api/v1/mocks/10001`

### Build an Image which already include all of your mocks

If you are in a company environment you may want to have an image which not only contains the mocking-server itself but
also all of your mocks. Therefore, you can create a dockerfile (example below), build your own image and push it into
your private docker registry.

```Dockerfile
FROM bosix/mocking-server
COPY mocks /app/mocks 
```

## Used 3rd Party Libraries

This app uses several 3rd party libraries. Their licenses can be
viewed [here](https://bosi.gitlab.io/mocking-server/licenses-3rd-party.txt) (gitlab pages).
